#include <bits/stdc++.h>
#define N_MAX 1005
#define INF 10000000
#define P pair<int, int>
#define ull unsigned long long int
using namespace std;

vector< P > g[N_MAX];
ull d[N_MAX];
bool vis[N_MAX];
int n, m, source;

void dijkstra(){
  priority_queue< P, vector< P >, greater< P > > qq;
  for(int i = 0; i < g[source].size(); i++){
    d[g[source][i].second] = g[source][i].first;
    qq.push(g[source][i]);
  }
  while(!qq.empty()){
    int u = qq.top().second;
    qq.pop();
    for(int i = 0; i < g[u].size(); i++){
      if(!vis[g[u][i].second] || d[g[u][i].second] > d[u] + g[u][i].first){
        qq.push(g[u][i]);
        d[g[u][i].second] = min(d[g[u][i].second], d[u] + g[u][i].first);
      }
    }
    vis[u] = true;
  }
}

int main(){
  ios_base::sync_with_stdio(false);
  cin >> n >> m;
  int x, y, z;
  memset(vis, 0, sizeof(vis));
  for(int i = 1; i <= n; i++)
    d[i] = INF;

  for(int i = 0; i < m; i++){
    cin >> x >> y >> z;
    g[x].push_back(make_pair(z, y));
    g[y].push_back(make_pair(z, x));
  }

  cin >> source;
  d[source] = 0;
  dijkstra();
  ull mi = INF, ma = 0;
  // for(int i = 1; i <= n; i++)
  //   cout << d[i] << ' ';
  // cout << '\n';
  for(int i = 1; i <= n; i++){
    if(i == source)
      continue;
    mi = min(mi, d[i]);
    ma = max(ma, d[i]);
  }

  cout << ma - mi << '\n';

  return 0;
}
