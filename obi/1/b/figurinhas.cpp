#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);

  int n, c, m;
  cin >> n >> c >> m;
  bool esp[n+1];
  bool card[n+1];
  memset(esp, 0, sizeof(esp));
  memset(card, 0, sizeof(card));
  int x;
  for(int i = 0; i < c; i++){
    cin >> x;
    if(x >= 1 && x <= n)
      esp[x] = true;
  }
  for(int i = 0; i < m; i++){
    cin >> x;
    if(x >= 1 && x <= n)
      card[x] = true;
  }

  int left = c;
  for(int i = 1; i <= n; i++){
    if(esp[i])
      if(card[i])
        left--;
  }

  cout << left << '\n';

  return 0;
}
