#include <bits/stdc++.h>
#define N_MAX 1000010
using namespace std;

int arr[N_MAX];
int ans[N_MAX];
int total, n;

int maxi(int i){
  if (i >= n)
    return 0;
  if (ans[i] != -1)
    return ans[i];

  ans[i] = max(
    maxi(i + 2),
    maxi(i + 1)
  );

  ans[i] = max(ans[i], arr[i]+maxi(i+3));
  return ans[i];
}

int main(){
  ios_base::sync_with_stdio(false);

  cin >> n;
  // memset(arr, 0, sizeof(arr));
  for(int i = 0; i < n; i++){
    cin >> arr[i];
    ans[i] = -1;
  }


  cout << maxi(0) << '\n';

}
