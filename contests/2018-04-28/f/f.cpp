#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

struct E {
  int d, t, v, i;
};

int ans[N_MAX];
int maxi[N_MAX];
E e[N_MAX];
int n;

int build(int i){
  // cout << i << '\n';
  // // cout << e[i].d - e[call].d << ' ' << e[i].t << '\n';
  // if (i >= n || e[i].d - e[call].d > e[i].t)
  //   return 0;
  // if (ans[i] != -1)
  //   return ans[i];

  ans[i] = e[i].v;
  int j = i+1;
  while(j < n && e[j].d - e[i].d <= e[j].t){
    ans[i] = max(ans[i], e[i].v + build(j));
    j++;
  }
  return ans[i];
}

int main(){
  ios_base::sync_with_stdio(false);
  int d, t, v;
  cin >> n;

  for(int i = 0; i < n; i++){
    cin >> d >> v >> t;
    e[i].d = d;
    e[i].v = v;
    e[i].t = t;
    e[i].i = i;
    maxi[i] = -1;
  }

  for(int i = 0; i < n;i++){
    build(i);
  }

  int answer = ans[0];
  for(int i = 1; i < n; i++)
    answer = max(answer, ans[i]);

  cout << answer << '\n';

  return 0;
}
