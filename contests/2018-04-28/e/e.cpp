#include <bits/stdc++.h>
using namespace std;

void swap(int &a, int &b){
  int temp = a;
  a = b;
  b = temp;
}

int partition(int arr[], int start, int end){
  int i, j, pivot = arr[end];
  i = j = start;

  int l = end-1;
  while (j < end) {

    if (arr[l] < pivot)
      swap(arr[l], arr[i++]);
    else
      swap(arr[l], arr[j]);

    j++;
  }
  swap(arr[i], arr[end]);
  return i;
}

int quickSelect(int arr[], int start, int end, int m){
  int p = partition(arr, start, end);
  if (p == m)
    return arr[p];
  if (p < m)
    return quickSelect(arr, p + 1, end, m);
  return quickSelect(arr, start, p-1, m);

}

int main(){
  ios_base::sync_with_stdio(false);

  int n,m;
  cin >> n >> m;
  int arr[n];
  for(int i = 0; i < n; i++)
    cin >> arr[i];

  cout << quickSelect(arr, 0, n-1, m-1) << '\n';

}
