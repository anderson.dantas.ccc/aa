#include <bits/stdc++.h>
using namespace std;

long long int m[26][26];

long long int change(string s, long long int i, long long int j){
  // cout << i << ' ' << j << '\n';
  long long int ans = min(m[i][j], m[j][i]);
  for(long long int k = 0; k < 26; k++){

    ans = min(ans, m[s[i] - 'a'][k] + m[s[j] - 'a'][k]);

  }
  return ans;
}
int main(){

  for(long long int i = 0; i < 26; i++){
    for(long long int j = 0; j < 26; j++){
      cin >> m[i][j];
    }
  }

  for(long long int k = 0; k < 26; k++){
    for(long long int i = 0; i < 26; i++){
      for(long long int j = 0; j < 26; j++)
        m[i][j] = min(m[i][j], m[i][k] + m[k][j]);
    }
  }

  string s;
  cin >> s;
  long long int ans = 0;
  for(long long int i = 0; i < (s.size() >> 1)+1; i++){
    if (s[i] != s[s.size()-1-i])
      ans += change(s, i, s.size()-1-i);
  }

  cout << ans << '\n';
  return 0;
}
