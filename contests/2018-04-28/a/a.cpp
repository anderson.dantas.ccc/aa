#include <bits/stdc++.h>
using namespace std;

long double tam(long double r, long double R, long double n){
  const long double PI = 3.1415926535897;
  return R * r * sin((PI/n))*n;
}

int main(){
  ios_base::sync_with_stdio(false);

  long double n, r,R;
  cin >> n >> R >> r;

  cout.precision(40);

  cout << tam(r, R, n) << '\n';

}
