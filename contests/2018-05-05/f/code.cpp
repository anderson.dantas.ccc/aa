#include <bits/stdc++.h>
#define ll long long int
#define N_MAX 100010
using namespace std;

int n;

ll acum[N_MAX];
ll a[N_MAX];

bool search(ll l, ll r, ll v){
  while (l <= r){
    int mid = (l+r) >> 1;
    if(acum[mid] == v)
      return true;
    if (acum[mid] > v)
      r = mid - 1;
    else
      l = mid + 1;
  }
  return false;
}


int main(){
  cin >> n;
  memset(acum, 0, sizeof(acum));
  cin >> a[0];
  acum[0] = a[0];
  for(int i = 1; i < n; i++){
    cin >> a[i];
    acum[i] = a[i] + acum[i-1];
  }

  ll tt = 0;
  ll mo = acum[n-1]/3;
  ll ans = 0;
  for(int i = 0; i < n && acum[i] <= mo; i++){
    if (search(i, n-1, acum[i] + mo) && search(i, n-1, acum[i] + (mo<<1)))
      ans++;
  }

  cout << ans << '\n';

  return 0;
}
