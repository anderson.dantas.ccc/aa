#include <bits/stdc++.h>
#define N_MAX 510
#define ll long long int
using namespace std;

struct Node {
  vector<int> parent;
  vector<int> child;
  ll v;
};

bool vis[N_MAX];
Node iks[N_MAX];

ll parent(int i, ll v){
  // cout << i << '\n';
  v = min(v, iks[i].v);
  vis[i] = true;
  for (int j = 0; j < iks[i].parent.size(); j++) {
    if (!vis[iks[i].parent[j]]) {
      v = min(v, parent(iks[i].parent[j], v));
    }
  }
  return v;
}

void swap_node(int x, int y){
  iks[x].child.swap(iks[y].child);
  iks[x].parent.swap(iks[y].parent);

  for(int i = 0; i < iks[x].child.size(); i++){
    int child = iks[x].child[i];
    bool found = false;
    for(int j = 0; j < iks[child].parent.size() && !found; j++){
      if (iks[child].parent[j] == y) {
        found = true;
        iks[child].parent.erase(iks[child].parent.begin()+j);
        if(child != x)
          iks[child].parent.push_back(x);
      }
    }
  }

  for(int i = 0; i < iks[y].child.size(); i++){
    int child = iks[y].child[i];
    bool found = false;
    for(int j = 0; j < iks[child].parent.size() && !found; j++){
      if (iks[child].parent[j] == x) {
        found = true;
        iks[child].parent.erase(iks[child].parent.begin()+j);
        if(child != y)
          iks[child].parent.push_back(y);
      }
    }
  }

  for(int i = 0; i < iks[y].parent.size(); i++){
    int parent = iks[y].parent[i];
    bool found = false;
    for(int j = 0; j < iks[parent].child.size() && !found; j++){
      if (iks[parent].child[j] == x) {
        found = true;
        iks[parent].child.erase(iks[parent].child.begin()+j);
        if(parent != y)
          iks[parent].child.push_back(y);
      }
    }
  }

  for(int i = 0; i < iks[x].parent.size(); i++){
    int parent = iks[x].parent[i];
    bool found = false;
    for(int j = 0; j < iks[parent].child.size() && !found; j++){
      if (iks[parent].child[j] == y) {
        found = true;
        iks[parent].child.erase(iks[parent].child.begin()+j);
        if(parent != x)
          iks[parent].child.push_back(x);
      }
    }
  }
}

int main(){
  ios_base::sync_with_stdio(false);
  int n, m, i, x, y;
  char op;
  cin >> n >> m >> i;
  for(int j = 1; j <= n; j++)
    cin >> iks[j].v;

  for(int j = 0; j < m; j++){
    cin >> x >> y;
    iks[x].child.push_back(y);
    iks[y].parent.push_back(x);
  }

  // for(int j = 1; j <= n; j++){
  //   cout << j << ' ' << iks[j].v << '\n';
  //   for(int k = 0; k < iks[j].parent.size(); k++)
  //     cout << iks[j].parent[k] << ' ';
  //   cout << '\n';
  //   for(int k = 0; k < iks[j].child.size(); k++)
  //     cout << iks[j].child[k] << ' ';
  //   cout << "\n--------\n";
  // }

  for(int j = 0; j < i; j++){
    cin >> op;
    if(op == 'P'){
      cin >> x;
      if (iks[x].parent.size() == 0)
        cout << "*\n";
      else {
        ll ans = 101;
        memset(vis, 0, sizeof(vis));

        for(int k = 0; k < iks[x].parent.size(); k++){
          ans = min(ans, parent(iks[x].parent[k], 101LL));
        }
        // cout << x << " ----------\n";
        cout << "P " << x << ": " << ans << '\n';
        // cout << ans << '\n';
      }
    } else if(op == 'T'){
      cin >> x >> y;
      swap_node(x, y);
    }
  }

  return 0;
}
