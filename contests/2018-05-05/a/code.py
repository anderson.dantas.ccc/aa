def p(a):
    if a == 0:
        return 'A'
    if a == 1:
        return 'B'
    if a == 2:
        return 'C'

a = map(int , raw_input().split())

if sum(a) == 3 or sum(a) == 0:
    print '*'
elif sum(a) == 1:
    for i in xrange(3):
        if a[i] == 1:
            print p(i)
elif sum(a) == 2:
    for i in xrange(3):
        if a[i] == 0:
            print p(i)
