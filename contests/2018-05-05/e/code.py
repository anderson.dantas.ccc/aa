a, b = map(int, raw_input().split())
if a == b:
    print '*'
    exit()
a = range(1, a+1)
c = map(int, raw_input().split())
for i in c:
    a.remove(i)
print ' '.join(map(str, a))
