#include <bits/stdc++.h>
#define MAXN 100010
using namespace std;

double a[MAXN];
double st[MAXN];
int n;

void update(int i, double v){
  while(i <= n){
    st[i] += v;
    i += (i & (-i));
  }
}

double query(int i){
  double sum = 0;
  while(i){
    sum += st[i];
    i -= (i & (-i));
  }
  return sum;
}

int main(){
  // cin.tie(0);
  // cout.tie(0);
  // ios_base::sync_with_stdio(0);

  cin >> n;

  for(int i = 1; i <= n; i++){
    scanf("%lf",&a[i]);
    update(i, a[i]);
  }

  int q;
  cin >> q;
  int p, i, j;
  double value;
  cout.precision(7);
  // answers = 0;
  vector<double> vvv;
  while(q--){
    cin >> p >> i;
    if(p & 1){
      cin >> value;
      update(i, value - a[i]);
      a[i] = value;
    } else {
      cin >> j;
      vvv.push_back((query(j) - query(i-1)) / (j-i+1));
    }
  }
  for(i = 0; i < vvv.size(); i++)
    printf("%lf\n", vvv[i]);
}
