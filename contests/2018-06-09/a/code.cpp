#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;
int a[N_MAX];
int main(){
  ios_base::sync_with_stdio(false);

  int n;
  cin >> n;
  int ans = 0;
  int resp = 0;
  while(n--){
    int x;
    cin >> x;
    ans += x;
    resp = min(resp, ans);
  }

  cout << max(-resp, 0) << '\n';

  return 0;
}
