#include <bits/stdc++.h>
#define N_MAX 1001
using namespace std;

struct Q{
  int i, v;
};

int max(Q a, Q b){
  if (a.i > b.i)
    return a.v;
  return b.v;
}

int main(){

  int n, m, k, p, q, r;
  cin >> n >> m >> k;
  Q row[n], col[m];
  Q emp;
  emp.i = -1;
  emp.v = 0;
  for(int i = 0; i < n; i++)
    row[i] = emp;
  for(int i = 0; i < m; i++)
    col[i] = emp;

  for(int i = 0; i < k; i++){
    cin >> p >> q >> r;
    Q a;
    a.i = i+1;
    a.v = r;
    if (p == 1)
      row[q-1] = a;
    else
      col[q-1] = a;
  }

  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      cout << max(row[i], col[j]);
      if (j+1 < m)
        cout << ' ';
    }
    cout << '\n';
  }

}
