x = int(raw_input())
m = {
    'Tetrahedron': 4,
    'Cube': 6,
    'Octahedron': 8,
    'Dodecahedron': 12,
    'Icosahedron': 20
}
ans = 0
for i in xrange(x):
    ans += m[raw_input()]
print '%d' % ans
