#include <bits/stdc++.h>
using namespace std;

int main(){
  int a;
  cin >> a;
  while (a--) {
    if (a%4<2)
      cout << 'a';
    else
      cout << 'b';
  }
  cout << '\n';
}
