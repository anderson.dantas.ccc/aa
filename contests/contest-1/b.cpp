#include <bits/stdc++.h>
#define N_MAX 1001
using namespace std;

int n, m, st[N_MAX][N_MAX];
int row[N_MAX][N_MAX];
int col[N_MAX][N_MAX];

int count_pos(int i, int j){
  int ans = 0;

  // direita
  if(row[i][m-1] - row[i][j])
    ans++;

  // esquerda
  if(row[i][j])
    ans++;

  // cima
  if(col[i][j])
    ans++;

  // baixo
  if(col[n-1][j] - col[i][j])
    ans++;
  // cout << i << ' ' << j << ' ' << row[i][m-1] + col[n-1][j] << '\n';

  return ans;
}

int main(){

  cin >> n >> m;
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++)
      cin >> st[i][j];
  }
  for(int i = 0; i < n; i++){
    row[i][0] = st[i][0];
    for(int j = 1; j < m; j++)
      row[i][j] = st[i][j] + row[i][j-1];
  }
  for(int j = 0; j < m; j++){
    col[0][j] = st[0][j];
    for(int i = 1; i < n; i++)
      col[i][j] = st[i][j] + col[i-1][j];
  }

  int ans = 0;
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      if (st[i][j] == 0)
        ans += count_pos(i, j);
    }
  }
  cout << ans << '\n';
}
