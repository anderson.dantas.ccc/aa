#include <bits/stdc++.h>
#define N_MAX 200010
#define ll long long
using namespace std;

ll n;
ll a[N_MAX];
bool m[N_MAX];

void swap(ll &x, ll &y){
  x ^= y;
  y ^= x;
  x ^= y;
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(false);
  memset(m, 0, sizeof(m));
  memset(a, 127, sizeof(a));

  cin >> n;
  for(ll i = 1; i <= n; i++)
    cin >> a[i];

  char c;
  for(ll i = 1; i < n; i++){
    cin >> c;
    if(c - '0')
      m[i] = true;
  }

  ll st = 1, stop = 1;
  for(ll i = 1; i < n; i++){
    while(i < n && !m[i]){
      if(a[i] != i && !m[i-1]){
        cout << "NO\n";
        return 0;
      }
      i++;
    }
    st = stop = i;
    while(i < n && m[i])
      i++, st++;
    for(int j = st; j >= stop; j--)
      if(a[j] > st || a[j] < stop){
        cout << "NO\n";
        return 0;
      }
  }
  cout << "YES\n";
  return 0;
}
