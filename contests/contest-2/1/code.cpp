#include <bits/stdc++.h>
#define N_MAX 1000010
#define ll long long
using namespace std;

ll b[N_MAX];
ll a[N_MAX];
ll f[N_MAX];
ll n;

void update(ll i, ll v){
  while(i <= n){
    b[i] += v;
    i += (i & (-i));
  }
}

ll query(ll i){
  ll sum = 0;
  while(i){
    sum += b[i];
    i -= (i & (-i));
  }
  return sum;
}

bool cmp(ll a, ll b){
  return a > b;
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(false);

  memset(b, 0, sizeof(b));
  memset(f, 0, sizeof(f));

  ll q;
  cin >> n >> q;
  for(ll i = 0; i < n; i++)
    cin >> a[i];
  sort(a, a+n, cmp);
  while(q--){
    ll l, r;
    cin >> l >> r;
    update(r+1, -1);
    update(l, 1);
  }

  ll sum = 0;
  for(ll i = 1; i <= n; i++){
    f[i-1] = query(i);
    // cout << i << ' ' << query(i) << '\n';
  }
  sort(f, f+n, cmp);

  for(ll i = 0; i < n; i++)
    sum += f[i] * a[i];
  cout << sum << '\n';

  return 0;
}
