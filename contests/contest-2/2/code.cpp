#include <bits/stdc++.h>
using namespace std;

struct Q{
  int l, r, i;
}q[1000];
int ans[1000];

bool cmp(Q a, Q b){
  if(a.l != b.l)
      return a.l < b.l;
  return a.i < b.i;
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(false);

  int t, n;
  cin >> t;
  while(t--){
    bool f[1000];
    memset(f, 0, sizeof(f));
    memset(ans, -1, sizeof(ans));
    cin >> n;
    for(int i = 0; i < n; i++){
      cin >> q[i].l >> q[i].r;
      // q[i].r -= q[i].l;
      q[i].i = i;
    }
    sort(q, q+n, cmp);
    int tmp = 0;

    for(int j = 0; j < n; j++){
      if(q[j].r > tmp)
        tmp = ans[q[j].i] = max(++tmp, q[j].l);
      else
        ans[q[j].i] = 0;
    }
    cout << ans[0];
    for(int j = 1; j < n; j++)
      cout << ' ' << ans[j];
    cout << '\n';
  }

  return 0;
}
