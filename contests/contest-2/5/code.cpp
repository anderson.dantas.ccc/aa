#include <bits/stdc++.h>
using namespace std;

#define MAXN 1010
#define mp(a, b) make_pair(a,b)
typedef pair<int, int> P;

int d[MAXN][MAXN];
bool connected[MAXN][MAXN];
bool vis[MAXN];
vector<int> g[MAXN];

void bfs(int o){
  memset(vis, 0, sizeof(vis));
  queue<P> q;
  q.push(mp(0, o));
  d[o][o] = 0;
  while(!q.empty()){
    P atual = q.front();
    q.pop();

    int v = atual.second, dist = atual.first;
    if(vis[v]) continue;

    vis[v] = true;
    d[o][v] = dist;
    for(auto u: g[v])
      if(!vis[u])
        q.push(mp(dist+1, u));
  }
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(false);
  int n, m, s, t;
  int x, y;
  cin >> n >> m >> s >> t;
  for(int i = 0; i <= n; i++)
    for(int j = 0; j <= n; j++)
      d[i][j] = MAXN;
  for(int i = 0; i < m; i++){
    cin >> x >> y;
    g[x].push_back(y);
    g[y].push_back(x);
    connected[x][y] = connected[y][x] = 1;
  }
  bfs(s);
  bfs(t);
  int cnt = 0;
  for(int i = 1; i <= n; i++)
    for(int j = i+1; j <= n; j++){
      if(!connected[i][j] && (d[s][i] + d[t][j] + 1 >= d[s][t]) && (d[s][j] + d[t][i] + 1 >= d[s][t]))
        cnt++;
    }
  cout << cnt << '\n';

  return 0;
}
