n, j = map(int, raw_input().split())

m = map(int, raw_input().split())
for i in xrange(1, n):
    m[i] += m[i-1]
for i in xrange(j):
    s = 0
    l, r = map(int, raw_input().split())
    s = m[r-1]
    if l != 1:
        s -= m[l-2]
    # print s
    if s % 2 == 0:
        print 'Sim'
    else:
        print 'Nao'
