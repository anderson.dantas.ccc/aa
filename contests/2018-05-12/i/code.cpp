#include <bits/stdc++.h>
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);
  int n, m;

  cin >> n >> m;
  int a[n];
  for(int i = 0; i < n; i++)
    cin >> a[i];

  int ya = 0, na = 0;

  int yac = 0, nac = 0;
  for(int i = 0; i < n; i++){
    // cout << "YAN " << i << ' ' << ya << '\n';
    if (ya + a[i] <= m) {
      ya += a[i];
      yac++;

    }
    else
      break;
  }
  for(int i = n-1; i >= 0; i--){
    // cout << "Nat " << i << ' ' << na << '\n';

    if (na + a[i] <= m) {
      na += a[i];
      nac++;
    }
    else
      break;
  }

  if (yac == nac)
    cout << "Empate\n";
  else if (yac > nac)
    cout << "Yan\n";
  else
    cout << "Nathan\n";


  return 0;
}
