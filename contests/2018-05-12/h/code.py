n = int(raw_input())

floor = map(int, raw_input().split())
foot = map(int, raw_input().split())
chain = map(int, raw_input().split())

ff = 0
for i in xrange(n):
    ff += abs(foot[i]-floor[i]) ** 2

fc = 0
for i in xrange(n):
    fc += abs(chain[i]-floor[i]) ** 2

if ff <= fc:
    print  'Yan'
else:
    print 'MaratonIME'
