from sys import stdout

n = raw_input()
ci = {}
oc = []
index = [i for i in xrange(len(n))]
oc.append(n[0])
ci[index[0]] = 1
dx = [index[0]]
for i in xrange(1, len(n)):
    if n[i] == n[i-1]:
        index[i] = index[i-1]
        ci[index[i]] += 1
    else:
        oc.append(n[i])
        ci[index[i]] = 1
        dx.append(index[i])
for i in xrange(len(oc)):
    stdout.write(oc[i])
    stdout.write(str(ci[dx[i]]))
stdout.write('\n')
stdout.flush()
