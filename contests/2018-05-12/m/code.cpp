#include <bits/stdc++.h>
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);

  int n, m;
  cin >> n >> m;
  char a[n][m];

  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++)
      cin >> a[i][j];
  }

  int ans = 0, acum = 0;

  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      if( a[i][j] == '.'){
        acum++;
        ans = max(acum, ans);
      } else if (a[i][j] == 'L')
        acum = 0;
    }
    i++;
    if (i < n) {
      for(int j = m-1; j >= 0; j--){
        if( a[i][j] == '.'){
          acum++;
          ans = max(acum, ans);
        } else if (a[i][j] == 'L')
          acum = 0;
      }
    }
  }

  cout << ans << '\n';

  return 0;
}
