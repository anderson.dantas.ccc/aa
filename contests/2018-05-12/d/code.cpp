#include <bits/stdc++.h>
using namespace std;

bool valid(int i, int j);
bool pawn(char m[8][8], int j, int i);
bool rook(char m[8][8], int j, int i);
bool bishop(char m[8][8], int j, int i);
bool horse(char m[8][8], int j, int i);
bool king(char m[8][8], int j, int i);

bool solve(char m[8][8], int j, int i){
  if (pawn(m, j, i))
    return true;

  if (rook(m, j, i))
    return true;

  if (bishop(m, j, i))
    return true;

  if (horse(m, j, i))
    return true;

  if (king(m, j, i))
    return true;
  return false;
}

bool pawn(char m[8][8], int j, int i){
  for(int k = j-1; k<= j+1; k++){
    if (!valid(i, k))
      continue;
    if (m[i][k] == 'P')
      return true;
  }
  return false;
}

bool rook(char m[8][8], int j, int i){
  for(int k = j+1; k < 8; k++){
    if (m[i][k] == 'T' || m[i][k] == 'R')
      return true;
    else if(m[i][k] != '.')
      break;
  }
  for(int k = j-1; k >= 0; k--){
    if (m[i][k] == 'T' || m[i][k] == 'R')
      return true;
    else if(m[i][k] != '.')
      break;
  }
  for(int k = i-1; k >= 0; k--){
    if (m[k][j] == 'T' || m[k][j] == 'R')
      return true;
    else if(m[k][j] != '.')
      break;
  }
  for(int k = i+1; k < 8; k++){
    if (m[k][j] == 'T' || m[k][j] == 'R')
      return true;
    else if(m[k][j] != '.')
      break;
  }

  return false;
}

bool bishop(char m[8][8], int j, int i){
  for(int x = 0; x < 8; x++){
    if(!valid(i + x, j + x))
      continue;
    if (m[i+x][j+x] == 'B' || m[i+x][j+x] == 'R')
      return true;
    else if(m[i+x][j+x] != '.')
        break;
  }
  for(int x = 0; x < 8; x++){
    if(!valid(i + x, j - x))
      continue;
    if (m[i+x][j-x] == 'B' || m[i+x][j-x] == 'R')
      return true;
    else if(m[i+x][j-x] != '.')
        break;
  }
  for(int x = 0; x < 8; x++){
    if(!valid(i - x, j - x))
      continue;
    if (m[i-x][j-x] == 'B' || m[i-x][j-x] == 'R')
      return true;
    else if(m[i-x][j-x] != '.')
        break;
  }
  for(int x = 0; x < 8; x++){
    if(!valid(i - x, j + x))
      continue;
    if (m[i-x][j+x] == 'B' || m[i-x][j+x] == 'R')
      return true;
    else if(m[i-x][j+x] != '.')
        break;
  }
  return false;
}

bool horse(char m[8][8], int j, int i){
  for(int x = -2; x <= 2; x+=4){
    for(int y = -1; y <=1; y += 2){
      if(!valid(i + x, j + y))
        continue;
      if(m[i+x][j+y] == 'C')
        return true;
    }
  }
  for(int x = -1; x <= 1; x+=2){
    for(int y = -2; y <=2; y += 4){
      if(!valid(i + x, j + y))
        continue;
      if(m[i+x][j+y] == 'C')
        return true;
    }
  }
  return false;
}

bool king(char m[8][8], int j, int i){
  for(int x = -1; x <= 1; x++){
    for(int y = -1; y <= 1; y++){
      if(!valid(i+x, j+y))
        continue;
      if(m[i+x][j+y] == 'R')
        return true;
    }
  }
  return false;
}
bool valid(int i, int j){
  if (i < 0 || i >= 8)
    return false;
  if (j < 0 || j >= 8)
    return false;
  return true;
}

bool isUpper(char a){
  return a >= 65 && a <=90;
}

int main(){
  ios_base::sync_with_stdio(false);

  char m[8][8];

  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++)
      cin >> m[i][j];
  }

  char col;
  int row;
  cin >> col >> row;

  if (solve(m, col - 'a', row-1))
    cout << "Sim\n";
  else
    cout << "Nao\n";

  return 0;
}
