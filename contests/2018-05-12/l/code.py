n = int(raw_input())
m = map(int, raw_input().split())
index = -2
mx = -1
for i in xrange(n):
    if m[i] == mx:
        index = -2
    if m[i] > mx:
        mx = m[i]
        index = i
print index + 1
