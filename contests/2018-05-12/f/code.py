from math import ceil,floor
n, m, k = map(int, raw_input().split())

v = float(k)/n
maxi = (n-m)/float(n) + v

ans = max(0, ceil((.7-v)*float(n)))
if (maxi >= .7):
    print '%d' % ans
else:
    print -1

print '%d' % int((n-m+k)*100/float(n))
