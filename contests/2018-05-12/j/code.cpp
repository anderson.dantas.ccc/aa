#include <bits/stdc++.h>
using namespace std;

struct P{
  int x, y, r;
};

bool intersect(P i, P j){
  if(pow(i.x-j.x, 2)+pow(j.y-i.y, 2) == pow((i.r+j.r),2))
    return true;
  return false;
}

int main(){
  ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  P m[n];

  for(int i = 0; i < n; i++)
    cin >> m[i].x >> m[i].y >> m[i].r;

  for(int i = 0; i < n; i++){
    for(int j = i+1; j < n; j++){
      if (intersect(m[i], m[j]))
        cout << i+1 << ' ' << j+1 << '\n';
    }
  }

  return 0;
}
