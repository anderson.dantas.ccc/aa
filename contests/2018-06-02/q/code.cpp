#include <bits/stdc++.h>
#define MAXN 100010
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);
  int n;
  int arr[MAXN];
  cin >> n;
  for(int i = 0; i < n; i++)
    cin >> arr[i];
  sort(arr, arr+n);
  int ans = arr[0] + arr[n-1];
  for(int i = 1; i <= n/2; i++)
    ans = min(ans, arr[i]+arr[n-1-i]);
  cout << ans << '\n';

  return 0;
}
