#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);
  int n, k;
  cin >> n >> k;
  int safe = 1, brok = k;
  int x;
  string op;
  while(n--){
    cin >> x >> op;
    if(op == "SAFE")
      safe = max(safe, x);
    else
      brok = min(brok, x);
  }
  cout << safe + 1 << ' ' << brok -1 << '\n';
  return 0;
}
