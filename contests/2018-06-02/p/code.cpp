#include <bits/stdc++.h>

using namespace std;

int arr[28];
string a;

int complexity(){
  set<int> c;
  for(int i = 0; i < a.size(); i++)
      arr[a[i] - 'a']++, c.insert(a[i]);
  return c.size();
}

int main(){
    cin >> a;
    int c = complexity();
    if(c <=2){
      cout << 0 << endl;
    } else {
      sort(arr, arr+28);
      int swamp = 0;
      int i = 0;
      while(c>2){
        if(arr[i]){
          c--;
          swamp+=arr[i];
        }
        i++;
      }
      cout << swamp << endl;
    }


    return 0;
}
