#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);
  int k;
  set<int> ans;
  string op[10];
  int v[10];
  cin >> k;
  for(int i = 0; i < k; i++)
    cin >> op[i] >> v[i];

  for(int j = 1; j <= 100; j++){
    int value = j;
    for(int i = 0; i < k; i++){
      if(op[i] == "MULTIPLY"){
        value *= v[i];
      }
      if(op[i] == "ADD"){
        value += v[i];
      }
      if(op[i] == "DIVIDE"){
        if(value % v[i]){
          ans.insert(j);
          break;
        }
        value /=v[i];
      }
      if(op[i] == "SUBTRACT"){
        if(value - v[i] < 0){
          ans.insert(j);
          break;
        }
        value -= v[i];
      }
    }
  }

  cout << ans.size() << '\n';

  return 0;
}
