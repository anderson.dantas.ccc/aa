#include <bits/stdc++.h>

#define MAXN 70

#define trace1(a) cout << a << '\n';
#define trace2(a, b) cout << a << ' ' << b << '\n';
#define trace3(a, b, c) cout << a << ' ' << b << ' ' << c << '\n';
#define trace4(a, b, c, d) cout << a << ' ' << b << ' ' << c << ' ' << d << '\n';
#define trace5(a, b, c, d, e) cout << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << '\n';
#define trace6(a, b, c, d, e, f) cout << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << ' ' << f << '\n';
#define printArr(harry, tam) loop1(0, tam-1) cout << harry[i] << " \n"[i == tam -1];

#define swap(a, b) auto temp = a; a = b; b = temp;

#define loop1(l, r) for(int i = l; i <= r; i++)
#define loop2(l, r) for(int j = l; j <= r; j++)
#define loop3(l, r) for(int k = l; k <= r; k++)
#define iloop1(l, r) for(int i = l; i >= r; i--)
#define iloop2(l, r) for(int j = l; j >= r; j--)
#define iloop3(l, r) for(int k = l; k >= r; k--)
#define range(sz) loop1(0, sz-1)

#define read(l, r, a) for(int i = l; i <= r; i++) scanf("%d", &a[i])

#define mp(a, b) make_pair(a, b);

using namespace std;

typedef long long ll;
typedef long double ld;

typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

typedef vector<ll> vll;
typedef vector<double> vd;

ll dp[MAXN][MAXN][2];
ll m;
int k, len;
vi digits;

ll solve(int pos, ll cnt, bool sml){
  if(pos == digits.size()) return cnt == 0;

  if(dp[pos][cnt][sml] != -1) return dp[pos][cnt][sml];

  ll ret = 0;
  ret += solve(pos+1, cnt, sml || digits[pos] == 1);

  if(cnt > 0 && (sml || digits[pos] == 1))
    ret += solve(pos+1, cnt-1, sml);

  return dp[pos][cnt][sml] = ret;
}

ll answer(ll v){
  memset(dp, -1, sizeof dp);
  digits.clear();
  while(v){
    digits.push_back(v&1);
    v>>=1;
  }
  reverse(digits.begin(), digits.end());
  return solve(0, k, 0);
}

ll bs(ll l, ll r){
  ll mid;
  while(l < r){
    mid = (l+r)/2;

    ll v = answer(2*mid) - answer(mid);

    if(v == m)
      return mid;
    if(v < m)
      l = mid+1;
    else
      r = mid-1;
  }
  return l;
}

int main(){

  cin >> m >> k;
  trace1(bs(1, 1000000000000000000LL));
  return 0;
}
