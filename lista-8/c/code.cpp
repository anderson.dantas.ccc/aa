#include <bits/stdc++.h>

#define MAXN 200010

#define trace1(a) cout << a << '\n';
#define trace2(a, b) cout << a << ' ' << b << '\n';
#define trace3(a, b, c) cout << a << ' ' << b << ' ' << c << '\n';
#define trace4(a, b, c, d) cout << a << ' ' << b << ' ' << c << ' ' << d << '\n';
#define trace5(a, b, c, d, e) cout << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << '\n';
#define trace6(a, b, c, d, e, f) cout << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << ' ' << f << '\n';
#define printArr(harry, tam) loop1(0, tam-1) printf("%lld%c", harry[i], " \n"[i == tam -1]);

#define swap(a, b) a^= b; b^= a; a^= b;
#define swap2(a, b) auto temp = a; a = b; b = temp;

#define loop1(l, r) for(int i = l; i <= r; i++)
#define loop2(l, r) for(int j = l; j <= r; j++)
#define loop3(l, r) for(int k = l; k <= r; k++)
#define loop4(l, r) for(int m = l; m <= r; m++)
#define loop5(l, r) for(int o = l; o <= r; o++)
#define loop6(l, r) for(int p = l; p <= r; p++)
#define iloop1(l, r) for(int i = l; i >= r; i--)
#define iloop2(l, r) for(int j = l; j >= r; j--)
#define iloop3(l, r) for(int k = l; k >= r; k--)
#define iloop4(l, r) for(int m = l; m >= r; m--)
#define iloop5(l, r) for(int o = l; o >= r; o--)
#define iloop6(l, r) for(int p = l; p >= r; p--)

#define read(l, r, a) for(int i = l; i <= r; i++) scanf("%lld", &a[i])
#define readM(l, r, ll, rr, a) for(int i = l; i <= r; i++) for(int j = ll; j <= rr; j++) scanf("%d", &a[i][j])
#define readGraph(l, r, g) for(int i = l; i <= r; i++){ int x, y; cin >> x >> y; g[x].push_back(y); g[y].push_back(x);}

#define pb(ve, value) ve.push_back(value);
#define mp(a, b) make_pair(a, b);
#define fis first;
#define sec second;

#define DIVIDER -9999999999LL

using namespace std;

typedef long long ll;
typedef long double ld;

typedef std::pair<int, int> ii;
typedef std::vector<int> vi;
typedef std::vector<ii> vii;
typedef std::vector<vi> vvi;
typedef std::vector<vii> vvii;

typedef std::vector<char> vc;
typedef std::vector<std::vector<char> > vvc;

typedef std::vector<ll> vll;
typedef std::vector<double> vd;

typedef std::string ss;
int n, m;
ll bear[MAXN];
ll elp[MAXN];

vll getPrefix(vll arr, int sz){
  vll pi(sz);

  pi[0] = 0;
  loop1(1, sz-1){
    int j = pi[i-1];

    while(j > 0 && arr[i] != arr[j])
      j = pi[j-1];

    if(arr[j] == arr[i])
      j++;

    pi[i] = j;
  }

  return pi;
}

int main(){
  cin >> n >> m;
  vll ma(n+m);
  read(0, n-1, bear);
  read(0, m-1, elp);

  iloop1(m-1, 1) ma[i-1] = elp[i] - elp[i-1];
  ma[m-1] = -9999999999LL;
  iloop1(n-1, 1) ma[m+i] = bear[i] - bear[i-1];

  vll pi = getPrefix(ma, n+m);

  int cnt = 0;

  loop1(m+1, m+n)
    if(pi[i] >= m-1)
      cnt++;

  trace1(cnt);
  // printArr(ma, n+m);
  // printArr(bear, n);
  // printArr(elp, m);

  return 0;
}
