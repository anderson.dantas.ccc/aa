#include <bits/stdc++.h>

#define MAXN 10000010

#define trace1(a) cout << a << '\n';
#define trace2(a, b) cout << a << ' ' << b << '\n';
#define trace3(a, b, c) cout << a << ' ' << b << ' ' << c << '\n';
#define trace4(a, b, c, d) cout << a << ' ' << b << ' ' << c << ' ' << d << '\n';
#define trace5(a, b, c, d, e) cout << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << '\n';
#define trace6(a, b, c, d, e, f) cout << a << ' ' << b << ' ' << c << ' ' << d << ' ' << e << ' ' << f << '\n';
#define printArr(harry, tam) loop1(0, tam-1) printf("%d%c", harry[i], " \n"[i == tam -1]);

#define swap(a, b) a^= b; b^= a; a^= b;
#define swap2(a, b) auto temp = a; a = b; b = temp;

#define loop1(l, r) for(int i = l; i <= r; i++)
#define loop2(l, r) for(int j = l; j <= r; j++)
#define loop3(l, r) for(int k = l; k <= r; k++)
#define loop4(l, r) for(int m = l; m <= r; m++)
#define loop5(l, r) for(int o = l; o <= r; o++)
#define loop6(l, r) for(int p = l; p <= r; p++)
#define iloop1(l, r) for(int i = l; i >= r; i--)
#define iloop2(l, r) for(int j = l; j >= r; j--)
#define iloop3(l, r) for(int k = l; k >= r; k--)
#define iloop4(l, r) for(int m = l; m >= r; m--)
#define iloop5(l, r) for(int o = l; o >= r; o--)
#define iloop6(l, r) for(int p = l; p >= r; p--)

#define read(l, r, a) for(int i = l; i <= r; i++) scanf("%d", &a[i])
#define readM(l, r, ll, rr, a) for(int i = l; i <= r; i++) for(int j = ll; j <= rr; j++) scanf("%d", &a[i][j])
#define readGraph(l, r, g) for(int i = l; i <= r; i++){ int x, y; cin >> x >> y; g[x].push_back(y); g[y].push_back(x);}

#define pb(ve, value) ve.push_back(value);
#define mp(a, b) make_pair(a, b);
#define fis first;
#define sec second;

using namespace std;

typedef long long ll;
typedef long double ld;

typedef std::pair<int, int> ii;
typedef std::vector<int> vi;
typedef std::vector<ii> vii;
typedef std::vector<vi> vvi;
typedef std::vector<vii> vvii;

typedef std::vector<char> vc;
typedef std::vector<std::vector<char> > vvc;

typedef std::vector<ll> vll;
typedef std::vector<double> vd;

typedef std::string ss;

char c[MAXN];
int pi[MAXN];

int main(){
  scanf(" %s", c);
  ss st = c;

  int n = st.size()-1;

  pi[0] = 0;
  loop1(1, n){
    int j = pi[i-1];

    while(j>0 && st[j] != st[i])
      j = pi[j-1];

    if(st[j] == st[i])
      j++;

    pi[i] = j;
  }

  bool ok = false;
  int pos = pi[n];
  while(pos > 0 && !ok){
    loop1(0, n-1)
      if(pi[i] == pos){
        ok = true;
        break;
      }
    if(!ok)
      pos = pi[pos-1];
  }

  if(ok){
    loop1(0,pos-1)
      printf("%c", st[i]);
    printf("\n");
  }
  else
    printf("Just a legend\n");

  return 0;
}
