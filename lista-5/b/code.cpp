#include <bits/stdc++.h>
#define N_MAX 100010
#define LOG 17
using namespace std;

struct Q{
  int  l, r, i;
};

vector<int> g[N_MAX];
int a[N_MAX];
int f[N_MAX];
int st[N_MAX];
int nd[N_MAX];
int euler[2*N_MAX];
int level[N_MAX];
int parent[N_MAX][LOG];
int ans[N_MAX];
Q q[N_MAX];
int n, m, SQRT;
int nextE = 0;

void swap(int &a, int &b){ a^=b; b^=a; a^=b; }

void dfs(int v, int p){
  level[v] = 1+level[p];
  parent[v][0] = p;
  st[v] = nextE;
  euler[v] = nextE++;
  for(auto u : g[v])
    if(u != p)
      dfs(u, v);
  nd[v] = nextE;
  euler[v] = nextE++;
}

int lca(int u , int v){
  if(level[u] < level[v]) swap(u, v);

  int dist = level[u] - level[v];
  while(dist > 0){
    int jump = log2(dist);
    u = parent[u][jump];
    dist -= (1<<jump);
  }
  if(u == v) return u;

  for(int j = LOG-1; j >= 0 ; --j)
    if((parent[u][j] != -1) && (parent[u][j] != parent[v][j]))
      u = parent[u][j], v = parent[v][j];
  return parent[u][0];
}

void build(int n){
  for(int j = 1; (1<<j) < n; j++)
    for(int i = 1; i <= n; i++)
      if (parent[i][j-1] != -1) parent[i][j] = parent[parent[i][j-1]][j-1];
}

bool inline cmp(Q a, Q b){
  if(a.l / SQRT ==  b.l / SQRT)
    return a.r < b.r;
  return a.l / SQRT < b.l / SQRT;
}

int answer = 0;

void inline add(int i){
  if(f[i] == 0)
    answer++;
  f[i]++;
}

void inline remove(int i){
  f[i]--;
  if(f[i] == 0)
    answer--;
}

void inline compute(int &L, int &R, int l, int r){
  while(R < r)
    add(++R);
  while(R > r)
    remove(R--);
  while(L < l)
    remove(L++);
  while(L > l)
    add(--L);
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(0);

  cin >> n >> m;
  SQRT = sqrt(n);

  for(int i = 1; i <= n; i++)
    cin >> a[i];
  int x, y;
  for(int i = 1; i < n; i++){
    cin >> x >> y;
    g[y].push_back(x);
    g[x].push_back(y);
  }
  level[1] = 0;
  dfs(1, 1);
  build(n);
  for(int i = 0; i < m; i++)
    cin >> q[i].l >> q[i].r, q[i].i=i;
  sort(q, q+m, cmp);

  int L=1, R=0;

  for(int i = 1; i <= m; i++){
    int l = q[i].l;
    int r = q[i].r;
    int index = q[i].i;

    int lowest = lca(l, r);

    compute(L, R, l, r);

    ans[index] = answer;
  }

  for(int i = 1; i <= m; i++)
    cout << ans[i] << '\n';
  return 0;
}
