#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

struct Q{
  int  l, r, i;
};

int a[N_MAX];
int f[N_MAX];
int ans[N_MAX];
Q q[N_MAX];
map <int, int> mapped;
int unmapped[N_MAX];
int n, m, SQRT;

bool inline cmp(Q a, Q b){
  if(a.l / SQRT ==  b.l / SQRT)
    return a.r < b.r;
  return a.l / SQRT < b.l / SQRT;
}

int answer = 0;

void inline add(int i){
  if(f[a[i]] == unmapped[a[i]])
    answer--;
  else if(f[a[i]]+1 == unmapped[a[i]])
    answer++;
  f[a[i]]++;
}

void inline remove(int i){
  if(f[a[i]] == unmapped[a[i]])
    answer--;
  else if(f[a[i]]-1 == unmapped[a[i]])
    answer++;
  f[a[i]]--;
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(0);

  cin >> n >> m;
  SQRT = sqrt(n);
  int nextPos=1, x;
  for(int i = 1; i <= n; i++){
    cin >> x;
    if(mapped[x])
      a[i] = mapped[x];
    else{
      mapped[x] = nextPos;
      a[i] = mapped[x];
      unmapped[nextPos++] = x;
    }
  }

  for(int i = 1; i <= m; i++)
    cin >> q[i].l >> q[i].r, q[i].i=i;
  sort(q, q+m, cmp);
  memset(f, 0, sizeof(f));

  int L=1, R=0;

  for(int i = 1; i <= m; i++){
    int l = q[i].l;
    int r = q[i].r;
    int index = q[i].i;

    while(R < r)
      add(++R);

    while(R > r)
      remove(R--);

    while(L < l)
      remove(L++);

    while(L > l)
      add(--L);

    ans[index] = answer;
  }

  for(int i = 1; i <= m; i++)
    cout << ans[i] << '\n';
  return 0;
}
