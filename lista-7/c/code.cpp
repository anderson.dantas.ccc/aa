#include <bits/stdc++.h>
#define N_MAX 50010
#define ll long long
using namespace std;

int ans;
struct Trie{
  int cnt;
  map<char, Trie> node;
  Trie(){
    cnt = 0;
  }
  void add(string &s, int i = 0){
    cnt++;
    if(i == s.size())
      ans = max(ans, (i)*cnt);
    else {
      ans = max(ans, (i)*cnt);
      this->node[s[i]].add(s, i+1);
    }
  }
};

bool cmp(string &a, string &b){
  return a.size() < b.size();
}

int main(){
  ios_base::sync_with_stdio(false);
  int t;
  cin >> t;
  while(t--){
    int n;
    cin >> n;
    int i;
    ans = 0;
    string number[n];
    for(i = 0; i < n; i++)
      cin >> number[i];
    Trie arr;
    for(i = 0; i < n; i++)
      arr.add(number[i]);
    cout << ans << '\n';
  }

  return 0;
}
