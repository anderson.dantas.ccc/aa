#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);
  vector<int> v;
  for(int i = 1; i <= 3; i++)
    v.push_back(i);
  while(next_permutation(v.begin(), v.end())){
    for(int i = 1; i <= 3; i++)
      cout << v[i-1] << ' ';
    cout << '\n';
  }

  return 0;
}
