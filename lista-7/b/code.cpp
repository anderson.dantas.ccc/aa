#include <bits/stdc++.h>
#define N_MAX 100010
using namespace std;

bool ok = true;

struct Trie{
  bool endpoint;
  map<char, Trie> node;
  Trie(){
    endpoint = 0;
  }
  void add(string &s, int i = 0){
    if(i == s.size())
      this->foo == (*(this)).bá
      this->endpoint = 1;
    else
      this->node[s[i]].add(s, i+1);
  }
};

bool cmp(string &a, string &b){
  return a.size() < b.size();
}

int main(){
  ios_base::sync_with_stdio(false);
  int t;
  cin >> t;
  while(t--){
    int n;
    cin >> n;
    int i;
    ok = true;
    string number[n];
    for(i = 0; i < n; i++)
      cin >> number[i];
    sort(number, number+n, cmp);
    Trie arr;
    for(i = 0; i < n; i++)
      arr.add(number[i]);
    if(ok)
      cout << "YES\n";
    else
      cout << "NO\n";
  }

  return 0;
}
