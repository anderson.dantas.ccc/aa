#include <bits/stdc++.h>
// #define MAXN 50010
#define MAXN 5000010

using namespace std;
typedef long long ll;

struct myhash {
	ll chash[MAXN];
	ll mult[MAXN];
	ll b1, M1;

	inline myhash(ll b1 = 257, ll M1 = 1000000009) :
		b1(b1), M1(M1) { calc();}

	inline void calc() {
		mult[0] = 1;
		for(ll i=1; i < MAXN; i++)
			mult[i] = (mult[i-1] * b1)%M1;
	}

	// Preprocess new string in global string st
	inline void compute(string &st) {
		chash[0] = chash[0] = st[0] + 1;
		for(int i=1; i<(int)st.size();i++)
			chash[i] = ((chash[i-1]*b1)%M1 + st[i] + 1)%M1;
	}

	inline ll substr(ll l, ll r) {
		if(!l) return chash[r];

		ll p1 = (chash[r] - (chash[l-1] * mult[r-l+1])%M1 + M1)%M1;
		return p1;
	}
};

string st;
string inv;
int p[MAXN];
char s[MAXN];
int cnt = 1;

int main() {
  // cin.tie(0);
  // cout.tie(0);
  // ios_base::sync_with_stdio(0);
	scanf(" %s", s);
	st = s;
	inv = string(st.rbegin(), st.rend());
	myhash g;
  myhash h;
	g.compute(st);
	h.compute(inv);
	int sz = st.size();
	p[0] = 1;

	for(int i = 1; i < sz; i++){
		// cout << i << '\n';
		ll p1 = g.substr(0, i);
		ll p2 = h.substr(sz-1-i, sz-1);
		if(p1 == p2){
			if(i&1)
				p[i] = p[i/2] + 1;
			else
				p[i] = p[(i-1)/2] + 1;
			cnt += p[i];
		}
	}

	printf("%d\n", cnt);
	// cout << cnt << '\n';

	return 0;
}
