#include <bits/stdc++.h>
#define MAXN 350
#define ll long long
using namespace std;

bool p[MAXN];
int cnt = 0;

void sieve(){
  memset(p, 0, sizeof(p));
  for(ll i = 2; i < MAXN;){
    cnt++;
    ll j = i<<1;
    while(j < MAXN){
      p[j] = 1;
      j+=i;
    }
    i++;
    while(i < MAXN && p[i])
      i++;
  }
}

int main(){
  sieve();
  for(ll i = 2; i < MAXN; i++)
    if(!p[i])
      printf("%lld ", i);
  // cout << a*b << endl;
  return 0;
}
// 1057489703
// 1057481927
