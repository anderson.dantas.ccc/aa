#include <bits/stdc++.h>
// #define MAXN 50010
#define MAXN 5000010
#define B1 313LL
#define B2 311LL
#define M1 1057489703LL
#define M2 1057481927LL
#define ll long long
#define trace1(a) printf("%lld\n", a)
#define trace2(a, b) printf("%lld %lld\n", a, b)
#define trace3(a, b, c) printf("%lld %lld %lld\n", a, b, c)
#define trace4(a, b, c, d) printf("%lld %lld %lld %lld\n", a, b, c, d)
#define trace5(a, b, c, d, e) printf("%lld %lld %lld %lld %lld\n", a, b, c, d, e)
using namespace std;

char s[MAXN];
ll p[MAXN];
string st;

int main(){

  // ll a = M2;
  // cout << a * a << '\n';

  scanf("%s", s);
  st = s;

  ll cnt = p[0] = 1;

  ll h1, h2, g1, g2, po1, po2;
  po2 = po1 = 1;
  g1 = h1 = s[0];
  g2 = h2 = s[0];
  p[0] = 1;

  for(ll i = 1; i < st.size(); i++){

    po1 = po1 * B1 % M1;
    g1 = (g1 + s[i] * po1 % M1) % M1;
    h1 = h1 * B1 % M1;
    h1 = (h1 + s[i]) % M1;

    po2 = po2 * B2 % M2;
    g2 = (g2 + s[i] * po2 % M2) % M2;
    h2 = h2 * B2 % M2;
    h2 = (h2 + s[i]) % M2;

		if(g1 == h1 && g2 == h2){
			p[i] = p[(i-1)>>1] + 1;
			cnt += p[i];
		}
    // trace2(h1, g1);
  }
  cout << cnt << '\n';
  return 0;
}
