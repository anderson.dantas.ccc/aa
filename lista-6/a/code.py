import math
while True:
    try:
        s = raw_input()
        a = [0 for i in xrange(30)]
        for i in s:
            a[ord(i)-65] += 1;
        numb = math.factorial(len(s))
        for i in a:
            numb /= math.factorial(i)
        numb %= 1000000007
        print(numb)
    except Exception as e:
        break
