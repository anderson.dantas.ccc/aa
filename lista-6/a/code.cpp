#include <bits/stdc++.h>
#define ll long long
// #define p 100 0000007LL
using namespace std;

ll pow(ll a, ll b, ll p){
  ll res = 1;
  while(b){
    if(b&1)
      res = ((res % p) * (a % p)) % p;

    b >>= 1;
    a = ((a % p) * (a % p)) % p;
  }
  return res;
}

int main(){
  ios_base::sync_with_stdio(false);
  cout << pow(2, 5, 13) << '\n';

  return 0;
}
