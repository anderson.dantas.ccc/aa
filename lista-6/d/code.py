# m = map(int, raw_input().split())
# a, b, c = map(int, raw_input().split())
# s = raw_input()
# l = raw_input().split()
# n = int(raw_input())
def gcd(a, b):
    if a == 0:
        return b
    return gcd(b%a, b)
case = 1
while True:
    try:
        a, b = map(int, raw_input().split())
        m = raw_input().split()

        for i in xrange(1, 10000):
            s = str(a*i)
            cont = False
            for j in m:
                if j in s:
                    cont = True
                    break
            if not cont:
                print "Case %d: %d" % (case, i * a)
                break
        case+=1

    except Exception as e:
        break
