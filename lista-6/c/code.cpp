#include <bits/stdc++.h>
#define MAXN 100010
#define ll long long
using namespace std;

bool p[MAXN];
ll x, y, d, r=-1, q=-1, tot, n, e, c;

void sieve(){
  memset(p, 0, sizeof(p));
  p[0] = p[1] = 1;
  for(ll i = 2; i < MAXN;){
    ll j = i+i;
    while(j < MAXN){
      p[j] = 1;
      j += i;
    }
    i++;
    while (i < MAXN && p[i] == 1)
      i++;
  }
}
inline bool isPrime(ll a){
  return p[a] == 0;
}
void factor(){
  for(ll i = 2; i < MAXN; i++)
    if(isPrime(i) && n % i == 0)
      if(r == -1)
        r = i;
      else {
        q = i;
        return;
      }
}

ll egcd(ll a, ll b, ll *x, ll *y){
  if (a == 0){
    *x = 0, *y = 1;
    return b;
  }

  ll x1, y1;
  ll gcd = egcd(b%a, a, &x1, &y1);

  *x = y1 - (b/a) * x1;
  *y = x1;

  return gcd;
}

ll modinv(ll a, ll m){
  ll x, y;
  egcd(a, m, &x, &y);

  return ((x%m) + m) %m;
}

ll pow(ll a, ll b, ll p){
  ll res = 1;
  while(b){
    if(b&1)
      res = ((res % p) * (a % p)) % p;
    b >>= 1;
    a = ((a % p) * (a % p)) % p;
  }
  return res;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin >> n >> e >> c;
  sieve();
  factor();
  tot = (r-1)*(q-1);
  ll d = modinv(e, tot);
  cout << pow(c, d, n) << '\n';

  return 0;
}
