#include <bits/stdc++.h>
#define ZERO 0
#define N_MAX 100002
#define BITS_C 64

using namespace std;

long long int st[4 * N_MAX+1];
long long int aux[4 * N_MAX+1];
long long int acum[4 * N_MAX+1];
long long int a[N_MAX+1];

void propagate(int id, int l, int r){
  st[id] ^= aux[id] & acum[id];

  if (l != r) {
    acum[id << 1] ^= acum[id];
    acum[(id << 1)|1] ^= acum[id];
  }
}

long long int read(long long int *b){
  long long int a = 0;
  for(int i = 0; i < BITS_C; i++)
    a ^= b[i];
  return a;
}
//
// void propagate(int id, int l, int r){
//   st[id] ^= (r-l+1) * acum[id];
//
//   if (l != r) {
//     acum[id << 1] += acum[id];
//     acum[(id << 1)|1] += acum[id];
//   }
//   acum[id] = ZERO;
// }
//
void build(int id, int l, int r){
  if(l == r){
    st[id] = a[l];
    aux[id] = a[l];
    return;
  }
  int m = (l+r)>>1;
  build(id<<1, l, m);
  build(1|(id<<1), m+1, r);
  st[id] = st[id<<1] + st[1|(id<<1)];
  aux[id] = ~(st[id<<1] ^ st[1|(id<<1)]);
}

void update(int id, long long int l, long long int r, int i, int j, long long int v){
  if (acum[id])
    propagate(id, l, r);

  if (j < l || i > r)
    return;
  if (i <= l && r <= j){
    st[id] ^= ~aux[id] & v;

    if (l != r) {
      acum[id << 1] ^= v;
      acum[(id << 1)|1] = v;
    }

    return;
  }
  int m = (l+r)>>1;
  update(id << 1, l, m, i, j, v);
  update((id << 1)|1, m+1, r, i, j, v);
  st[id] = st[id<<1] + st[(id<<1)|1];
  aux[id] = ~(st[id<<1] ^ st[(id<<1)|1]);
}
//
// void update(int id, long long int l, long long int r, int i, int j, long long int v){
//   if (acum[id])
//     propagate(id, l, r);
//
//   if (j < l || i > r)
//     return;
//   if (i <= l && r <= j){
//     st[id] ^= (r-l+1) * v;
//
//     if (l != r) {
//       int m = (l+r)>>1;
//       if (acum[id << 1])
//         propagate(id << 1, l, m);
//       acum[id << 1] = v;
//
//       if (acum[1|(id << 1)])
//         propagate(1|(id << 1), m+1, r);
//       acum[(id << 1)|1] = v;
//     }
//
//     return;
//   }
//   int m = (l+r)>>1;
//   update(id << 1, l, m, i, j, v);
//   update((id << 1)|1, m+1, r, i, j, v);
//   st[id] = st[id<<1] + st[(id<<1)|1];
// }
//
long long int *query(int id, long long int l, long long int r, int i, int j){
  // cout << id << ' ' << l << ' ' << r << ' ' << i << ' ' << j << ' ' << '\n';
  if (acum[id])
    propagate(id, l, r);

  if (j < l || i > r){
    return ZERO;
  }
  if (i <= l && r <= j)
    return st[id];

  int m = (l+r)>>1;
  return (
    query(id << 1, l, m, i, j) +
    query((id << 1)|1, m+1, r, i, j)
  );
}

int main(){
  ios_base::sync_with_stdio(false);

  int t, n, c, l, r, op;
  long long int v;

  cin >> n;
  //
  // memset(st, 0, sizeof(st));
  // memset(a, 0, sizeof(a));
  // memset(acum, 0, sizeof(acum));
  long long int inp;
  for(int z = 1; z <= n; z++){
    cin >> inp;
    copy(A[z], inp);
  }

  build(1, 1, n);

  cin >> t;

  for(int z = 0; z < t; z++){
    cin >> op >> l >> r;
    if (op == 2){
      cin >> v;
      update(1, 1, n, l, r, v);
    } else {
      // printf("%lld\n", query(1, 1, n, p, q));
      cout << query(1, 1, n, l, r) << '\n';

    }
  }

  return 0;
}
