#include <bits/stdc++.h>
#define K 16
#define N_MAX 100001

using namespace std;

int A[N_MAX];
int lookup[N_MAX][K];

int m, n, x, y;

void build(int n){
  for(int i = 0; i < n; i++){
    lookup[i][0] = A[i];
  }

  for(int j = 1; (1 << j) <= n; j++){
    for(int i = 0; i + (1 << j) - 1 < n; i++)
        lookup[i][j] = max(lookup[i][j-1], lookup[i + (1 << (j - 1))][j-1]);
  }
}

int query(int l, int r){
  int j = (int) floor(log2(r-l+1));

  return max(lookup[l][j], lookup[r - (1 << j) + 1][j]);
}

int main(){
  ios_base::sync_with_stdio(false);
  cin >> n;

  for(int i = 0; i < n; i++)
    cin >> A[i];
  build(n);

  cin >> m >> x >> y;

  int ans = 0;

  for(int z = 0; z < m; z++){
    ans += query(min(x, y), max(x, y));
    x = (x+7) % (n-1);
    y = (y+11) % n;
  }

  cout << ans << endl;

  return 0;
}
