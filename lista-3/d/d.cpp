#include <bits/stdc++.h>
#define P pair<LL, LL>
#define N_MAX 100010
#define INF 10000000000
#define LOG 17
#define LL long long int
using namespace std;

vector< vector<P> > g(N_MAX);
LL level[N_MAX] = {0};
LL parent[N_MAX][LOG] = {0};
LL mini[N_MAX][LOG] = {0};
LL maxi[N_MAX][LOG] = {0};
bool vis[N_MAX] = {false};
LL root;

void swap(LL &a, LL &b){
  a^=b;
  b^=a;
  a^=b;
}

void dfs(LL u, LL p, LL c){
  vis[u] = true;
  level[u] = level[p] + 1;
  parent[u][0] = p;
  mini[u][0] = c;
  maxi[u][0] = c;
  for(LL i = 0; i < g[u].size(); i++){
    LL v = g[u][i].first;
    if (!vis[v] && v != p) {
      dfs(v, u, g[u][i].second);
    }
  }
}

void build(LL n){
  for(LL j = 1; (1<<j) < n; j++){
    for(LL i = 1; i <= n; i++){
      if (parent[i][j-1] != -1)
        parent[i][j] = parent[parent[i][j-1]][j-1];

      if (mini[parent[i][j-1]][j-1] != -1)
        mini[i][j] = min(mini[parent[i][j-1]][j-1], mini[i][j-1]);

      if (maxi[parent[i][j-1]][j-1] != -1)
        maxi[i][j] = max(maxi[parent[i][j-1]][j-1], maxi[i][j-1]);
    }
  }
}

LL lca(LL u , LL v){
  if(level[u] < level[v]){
    swap(u, v);
  }

  LL dist = level[u] - level[v] ;
  while(dist > 0){
    LL jump = log2(dist);
    u = parent[u][jump];
    dist -= (1<<jump);
  }

  if(u == v){
    return u ;
  }

  for(LL j = LOG-1; j >= 0 ; --j){
    if((parent[u][j] != -1) && (parent[u][j] != parent[v][j])){
      u = parent[u][j];
      v = parent[v][j];
    }
  }
  return parent[u][0];
}

LL countMin(LL a, LL l){
  LL resp = INF;
  while(l > 0){
    LL jump = log2(l);
    resp = min(resp, mini[a][jump]);
    a = parent[a][jump];
    l -= (1<<jump);
  }
  // return min(resp, mini[a][0]);
  return resp;
}

LL countMax(LL a, LL l){
  LL resp = 0;
  while(l > 0){
    LL jump = log2(l);
    resp = max(resp, maxi[a][jump]);
    a = parent[a][jump];
    l -= (1<<jump);
  }
  // return max(resp, maxi[a][0]);
  return resp;
}

int main(){
  ios_base::sync_with_stdio(false);

  for(LL j = 0; j < LOG; j++){
    for(LL i = 0; i < N_MAX; i++){
      parent[i][j] = -1;
      mini[i][j] = -1;
      maxi[i][j] = -1;
    }
  }
  LL n;
  cin >> n;

  LL a, b, k;

  for(LL i = 0; i < n-1; i++){
    cin >> a >> b >> k;
    if(i == 0){
      root = a;
      parent[a][0] = a;
      mini[a][0] = INF;
      maxi[a][0] = 0;
      level[a] = 0;
    }
    g[a].push_back(make_pair(b, k));
    g[b].push_back(make_pair(a, k));
  }

  for(LL i = 0; i < g[root].size(); i++){

    dfs(g[root][i].first, root, g[root][i].second);
  }

  build(n);

  LL q;
  cin >> q;
  while (q--) {
      cin >> a >> b;
      LL ans = lca(a, b);
      cout << min(
        countMin(a, level[a] - level[ans]),
        countMin(b, level[b] - level[ans])) << ' ';
      cout << max(
        countMax(a, level[a] - level[ans]),
        countMax(b, level[b] - level[ans])) << '\n';
  }
  return 0;
}
