#include <bits/stdc++.h>
#define P pair<int, int>
#define LOG 14
using namespace std;

vector< vector<P> > g(10020);
int level[10020] = {0};
int parent[10020][LOG] = {0};
long long int cost[10020][LOG] = {0};
bool vis[10020] = {false};
int root;

void swap(int &a, int &b){
  a^=b;
  b^=a;
  a^=b;
}

void dfs(int u, int p, int c){
  vis[u] = true;
  level[u] = level[p] + 1;
  parent[u][0] = p;
  cost[u][0] = c;
  for(int i = 0; i < g[u].size(); i++){
    int v = g[u][i].first;
    if (!vis[v] && v != p) {
      dfs(v, u, g[u][i].second);
    }
  }
}

void build(int n){
  for(int j = 1; (1<<j) < n; j++){
    for(int i = 1; i <= n; i++){
      if (parent[i][j-1] != -1)
        parent[i][j] = parent[parent[i][j-1]][j-1];

      if (cost[parent[i][j-1]][j-1] != 0 || cost[i][j-1] != 0)
        cost[i][j] = cost[parent[i][j-1]][j-1] + cost[i][j-1];
    }
  }
}

int lca(int u , int v){
  if(level[u] < level[v]){
    swap(u, v);
  }

  int dist = level[u] - level[v] ;
  while(dist > 0){
    int jump = log2(dist);
    u = parent[u][jump];
    dist -= (1<<jump);
  }

  if(u == v){
    return u ;
  }

  for(int j = LOG-1; j >= 0 ; --j){
    // cout << j << ' ' << u << ' ' << v << '\n';
    if((parent[u][j] != -1) && (parent[u][j] != parent[v][j])){
      u = parent[u][j];
      v = parent[v][j];
    }
  }
  return parent[u][0];
}

int node_at_distance(int u, int k){
  while(k > 0){
    int jump = log2(k);
    u = parent[u][jump];
    k -= (1<<jump);
  }
  return u;
}

int kth(int u, int v, int k){
  int ans = lca(u, v);
  if (level[u] - level[ans] + 1 == k){
    // cout << "1: ";
    return ans;
  }
  else if(level[u] - level[ans] + 1 > k){
    // cout << "2: ";
    return node_at_distance(u, k-1);
  }
  // cout << "lca("<<u<<','<<v<<")= " << lca(u, v) <<"; 3: ";
  return node_at_distance(v, (level[v] - level[ans]) - (k - (level[u] - level[ans] + 1)));
  // return node_at_distance(v, (level[u]<<1) - k);
}

long long int dist_to_root(int a){
  int l = level[a];
  long long resp = 0;
  while(l > 0){
    int jump = log2(l);
    resp += cost[a][jump];
    a = parent[a][jump];
    l -= (1<<jump);
  }
  return resp + cost[a][0];
}

long long int dista(int a, int b){
  return dist_to_root(a) + dist_to_root(b) - (dist_to_root(lca(a,b)) << 1);
}
int main(){
  ios_base::sync_with_stdio(false);

  int t;
  cin >> t;
  while (t--) {
      for(int j = 0; j < LOG; j++){
        for(int i = 0; i < 10020; i++){
          parent[i][j] = -1;
          cost[i][j] = 0;
        }
      }
    int n;
    cin >> n;

    int a, b, k;

    for(int i = 0; i < n-1; i++){
      cin >> a >> b >> k;
      if(i == 0){
        root = a;
        parent[a][0] = a;
        cost[a][0] = 0;
        level[a] = 0;
      }
      g[a].push_back(make_pair(b, k));
      g[b].push_back(make_pair(a, k));
    }

    for(int i = 0; i < g[root].size(); i++){

      dfs(g[root][i].first, root, g[root][i].second);
    }

    build(n);

    string op = "";
    cin >> op;
    while (op != "DONE") {
      if (op == "DIST") {
        cin >> a >> b;
        cout << dista(a, b) << '\n';
        // cout << lca(a, b) << '\n';
      } else if (op == "KTH") {
        cin >> a >> b >> k;
        cout << kth(a, b, k) << '\n';
      }
      cin >> op;
    }
    cout << '\n';

    for(int i = 1; i <=n && t > 0; i++){
      // cout << g[i].size() << '\n';
      level[i] = 0;
      vis[i] = false;
      g[i].clear();
    }
  }
  return 0;
}
