#include<bits/stdc++.h>
#define P pair;
#define N_MAX 200010
using namespace std;

int arr[N_MAX];
bool vis[N_MAX];
bool toggle[N_MAX];
int start[N_MAX];
int nd[N_MAX];
int tour[2*N_MAX];
int nextPos = 0;
int st[N_MAX];

vector< vector<int> > g(N_MAX);

int get(int node, int l, int r, int s, int e);

void dfs(int i){
  vis[i] = true;
  tour[nextPos++] = i;
  for(int j = 0; j < g[i].size(); j++){
    if (!vis[g[i][j]])
      dfs(g[i][j]);
  }
  tour[nextPos++] = i;
}

int build(int n){
  toggle[n] = false;
  st[n] = arr[n];
  for(int i = 0; i < g[n].size(); i++)
    st[n] += build(g[n][i]);
  return st[n];
}

void propagate(int node){
  st[node] = ((nd[node]-start[node]+1)>>1) - st[node];
  for(int i = 0; i < g[node].size(); i++)
    toggle[g[node][i]] = !toggle[g[node][i]];
  toggle[node] = false;
}

void pow(int node, int l, int r, int s, int e, int target){
  if (toggle[node])
    propagate(node);

  if (node == target){
    st[node] = ((r-l+1)>>1) - st[node];
    for(int i = 0; i < g[node].size(); i++)
      toggle[g[node][i]] = !toggle[g[node][i]];
    toggle[node] = false;
    return;
  }

  for(int i = 0; i < g[node].size(); i++){
    if (start[g[node][i]] <= s && nd[g[node][i]] >= e)
      pow(g[node][i], start[g[node][i]], nd[g[node][i]], s, e, target);
  }

  st[node] = arr[node];
  for(int i = 0; i < g[node].size(); i++)
    st[node] += get(node, start[node], nd[node], start[g[node][i]], nd[g[node][i]]);
}

int get(int node, int l, int r, int s, int e){
  // cout << node << ' ' << l << ' ' << r << ' ' << s << ' ' << e << '\n';
  if (toggle[node])
    propagate(node);

  if (l == s && r == e)
    return st[node];

  for(int i = 0; i < g[node].size(); i++){
    if (start[g[node][i]] <= s && nd[g[node][i]] >= e)
      return get(g[node][i], start[g[node][i]], nd[g[node][i]], s, e);
  }

  return 0;
}

int main(){
  ios_base::sync_with_stdio(false);

  int n, q;
  cin >> n;

  int p;
  for(int i = 0; i < n-1;i++){
    cin >> p;
    g[p].push_back(i+2);
  }

  for(int i = 1; i <= n;i++){
    start[i] = -1;
    cin >> arr[i];
  }

  tour[nextPos++] = 1;
  for(int j = 0; j < g[1].size(); j++){
    dfs(g[1][j]);
  }
  tour[nextPos++] = 1;

  for(int i = 0; i < (n<<1); i++){
    if (start[tour[i]] == -1)
      start[tour[i]] = i;
    else
      nd[tour[i]] = i;
  }

  build(1);

  string op;
  int node;
  cin >> q;
  while (q--) {
    cin >> op >> node;
    if (op == "pow"){
      toggle[node] = !toggle[node];
      pow(1, start[1], nd[1], start[node], nd[node], node);
    }
    else
      cout << get(1, start[1], nd[1], start[node], nd[node]) << '\n';
  }

  return 0;
}
