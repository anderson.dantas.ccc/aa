#include <bits/stdc++.h>
#define P pair<int, int>
#define N_MAX 500010
using namespace std;

vector<int> g[N_MAX];
vector<P> query[N_MAX];
int level[N_MAX];
int a[N_MAX];
int o[N_MAX];
int sz[N_MAX];
bool ans[N_MAX];
bool big[N_MAX];
int n, q;

int count_level(int v, int parent){
  level[v] = 1+level[parent];
  sz[v] = 1;
  for(auto u: g[v])
    if(u != parent){
      count_level(u, v);
      sz[v] += sz[u];
    }
  return sz[v];
}

void add(int v, int parent){
  o[level[v]] ^= a[v];
  for(auto u: g[v])
    if(u != parent && !big[u])
      add(u, v);
}

void sack(int v, int parent, bool keep){
  int mx = -1, bigger=-1;
  for(auto u: g[v])
    if(u != parent && sz[u]>mx)
      mx=sz[u], bigger=u;
  for(auto u: g[v])
    if(u != parent && u != bigger)
      sack(u, v, 0);
  if(bigger != -1)
    sack(bigger, v, 1), big[bigger] = true;
  add(v, parent);

  for(auto qq: query[v]){
    if(o[qq.first] == (o[qq.first] & -o[qq.first])){
      ans[qq.second] = true;
    }
  }

  if(bigger != -1)
    big[bigger] = 0;
  if(!keep)
    add(v, parent);
}

int main(){
  cin.tie(NULL);
  cout.tie(NULL);
  ios_base::sync_with_stdio(false);

  cin >> n >> q;
  memset(level, 0, sizeof(level));
  memset(ans, 0, sizeof(ans));
  memset(big, 0, sizeof(big));
  memset(o, 0, sizeof(o));
  int v;
  for(int i = 2; i <= n; i++){
    cin >> v;
    g[v].push_back(i);
    g[i].push_back(v);
  }
  char c;
  for(int i = 1; i <= n; i++){
    cin >> c;
    a[i] = 1 << (c-'a');
  }
  int node, th;
  for(int i = 0; i < q; i++){
    cin >> node >> th;
    query[node].push_back(make_pair(th, i));
  }
  level[0] = 0;
  level[1] = 1;
  count_level(1, 0);

  sack(1, 0, 1);

  for(int i = 0; i < q; i++){
    if(ans[i])
      cout << "Yes\n";
    else
      cout << "No\n";
  }

  return 0;
}
//   if (i == (i & -i))
