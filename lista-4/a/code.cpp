#include <bits/stdc++.h>
#define ull unsigned long long
#define N_MAX 500010
using namespace std;

vector<int> g[N_MAX];
int f[N_MAX];
int col[N_MAX];
ull sum[N_MAX];
int f_max = 0;
int sz[N_MAX];
ull ans[N_MAX];
bool big[N_MAX];
int n, q;

int get_sz(int v, int parent){
  sz[v] = 1;
  for(auto u: g[v])
    if(u != parent)
      sz[v] += get_sz(u, v);
  return sz[v];
}

void add(int v, int parent, bool k){
  if (k) {
    sum[f[col[v]]] -= col[v];
    f[col[v]]++;
    sum[f[col[v]]] += col[v];
    if (f[col[v]] > f_max){
      f_max = f[col[v]];
    }
  } else {
    sum[f[col[v]]] -= col[v];
    f[col[v]]--;
    sum[f[col[v]]] += col[v];
    if (sum[f_max] == 0){
      f_max = f[col[v]];
    }
  }
  for(auto u: g[v])
    if(u != parent && !big[u])
      add(u, v, k);
}

void sack(int v, int parent, bool keep){
  int mx = -1, bigger=-1;
  for(auto u: g[v])
    if(u != parent && sz[u]>mx)
      mx=sz[u], bigger=u;
  for(auto u: g[v])
    if(u != parent && u != bigger)
      sack(u, v, 0);
  if(bigger != -1)
    sack(bigger, v, 1), big[bigger] = 1;
  add(v, parent, 1);

  ans[v] = sum[f_max];

  if(bigger != -1)
    big[bigger] = 0;
  if(!keep)
    add(v, parent, 0);
}

int main(){
  cin.tie(0);
  cout.tie(0);
  ios_base::sync_with_stdio(0);

  cin >> n;
  // memset(f, 0, sizeof(f));
  // memset(sum, 0, sizeof(sum));
  // memset(big, 0, sizeof(big));
  for(int i = 1; i <= n; i++)
    cin >> col[i];

  int x, y;
  for(int i = 1; i < n; i++){
    cin >> x >> y;
    g[x].push_back(y);
    g[y].push_back(x);
  }

  get_sz(1, 0);

  sack(1, 0, 1);

  cout << ans[1];
  for(int i = 2; i <= n; i++)
    cout << ' ' << ans[i];
  cout << '\n';


  return 0;
}
