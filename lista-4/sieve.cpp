#include <iostream>
#include <cstring>
#define N_MAX 1299709
using namespace std;

bool num[N_MAX+1];

int cnt = 0;

void sieve(){
  int i = 2;
  while(i <= N_MAX){
    cnt++;
    int j = i<<1;
    while(j <= N_MAX){
      num[j] = false;
      j += i;
    }
    i++;
    while(i <= N_MAX && !num[i])
      i++;
  }
}

int main(){
  memset(num, 1, sizeof(num));
  num[0] = false;
  num[1] = false;
  sieve();
  // cout << N_MAX << ' ';
  for(int i = 0; i <= N_MAX; i++){
    if(num[i])
      cout << i << ' ';
  }

  cout << "\nCount: " << cnt << '\n';
  return 0;
}
