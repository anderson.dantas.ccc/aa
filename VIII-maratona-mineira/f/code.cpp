#include <bits/stdc++.h>
#define PRIME 1000000009
#define ull unsigned long long
using namespace std;

int main(){
  ios_base::sync_with_stdio(false);

  int f = 0, m = 0, cnt = 0;
  string n;
  cin >> n;
  char c;

  for(int i = 0; i < n.size(); i++){
    c = n[i];
    if(c == 'F')
      f++;
    else
      m++;
    if(f == m)
      cnt++;
  }
  if(cnt == 0){
    cout << "1\n";
    return 0;
  }
  ull resp = 0;
  for(int i = 0; i < cnt; i++){
    resp = ((resp % PRIME) + (i % PRIME)) % PRIME;
  }
  if(cnt & 1)
    cout << resp + 1 << '\n';
  else
    cout << resp << '\n';

  return 0;
}
