#include <bits/stdc++.h>
#define N_MAX 1000010
#define ull unsigned long long
using namespace std;

int s[7], t;

int maxi(){
  // int ans = s[j];
  int j = 1;
  for(int i = 0; i <= 6; i++){
    if(s[i] > s[j])
      j = i;
  }
  return j;
}

int main(){
  ios_base::sync_with_stdio(false);
  memset(s, 0 , sizeof(s));
  // n = -1;
  int x;
  cin >> t;
  for(int i = 0; i < t; i++){
    cin >> x;
    s[x]++;
  }
  ull resp = t * t;
  ull tmp;
  // cout << resp;
  for(int i = 1; i < 7; i++){
    tmp = 0;
    for(int j = 1; j < 7; j++){
      if(i == j)
        continue;
      if(7 - j == i)
        tmp += s[j];
      tmp += s[j];
      // cout << i << ' ' << j << ' ' << tmp << '\n';
    }
    resp = min(resp, tmp);
  }

  cout << resp << '\n';
  return 0;
}
