#include <bits/stdc++.h>
#define M pair<int, int>
using namespace std;



int main(){
  ios_base::sync_with_stdio(false);
  int n;
  cin >> n;
  M m[n];
  for(int i = 0; i < n; i++)
    cin >> m[i].first >> m[i].second;
  sort(m, m+n);
  for(int i = 1; i < n; i++){
    if(m[i].second > m[i-1].second){
      cout << "N\n";
      return 0;
    }
  }
  cout << "S\n";

  return 0;
}
