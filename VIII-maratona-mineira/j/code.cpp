#include <bits/stdc++.h>
#define N_MAX 1010
using namespace std;

int n, m, c;
int a[N_MAX][N_MAX];

bool containsCol(int l){
  for(int i = 0; i < n; i++)
    if(a[i][l])
      return true;
  return false;
}
bool checkLine(int val){
  // cout << val << '\n';
  for(int j = 0; j < m; j++)
    for(int i = 0; i < n; i++){
      // cout << i << '\n';
      if(a[i][j] != val)
        continue;
      for(int line = 1; line <= c; line++){
        // cout << i << ' ' << line << '\n';
        if(i+line < n && a[i+line][j] == val)
          return true;
      }
      // i+= max(c-1, 0);
    }
  // cout << val << '\n';
  return false;

}
bool checkCol(){
  for(int i = 0; i < m-1; i++){
    if (containsCol(i)){
      if(i+1<m && containsCol(i+1))
        return true;
      else
        i++;
    }
    // cout << i << '\n';
  }
  return false;
}

int main(){
  ios_base::sync_with_stdio(false);

  cin >> n >> m >> c;
  // int a[n][m];
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      cin >> a[i][j];
  if(!checkCol() && !checkLine(1) && !checkLine(2))
    cout <<"S\n";
  else
    cout <<"N\n";

  return 0;
}
