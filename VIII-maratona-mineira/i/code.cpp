#include <bits/stdc++.h>
#define N_MAX 1010
#define K_MAX 1000010
using namespace std;

struct P{
  int c, l;
  bool invert = false;
};

bool cmp(P a, P b){
  return a.c * a.l > b.c * b.l;
}

int n, m;
char mapa[N_MAX][N_MAX];
int row[N_MAX][N_MAX];
int col[N_MAX][N_MAX];
int pos[N_MAX][N_MAX];
P moveis[K_MAX];

void set_row(){
  for(int i = 0; i < n; i++){
    row[i][0] = mapa[i][0] == '.' ? 1 : 0;
    for(int j = 1; j < m; j++){
      if(mapa[i][j] == '.')
        row[i][j] = 1+row[i][j-1];
      else
        row[i][j] = 0;
    }
  }
}

void vizualize(){
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      if(row[i][j] < 10)
        cout << "  " << row[i][j];
      else
        cout << ' ' << row[i][j];
    }
    cout << '\n';
  }
  cout << "------------\n";
  for(int i = 0; i < n; i++){
    for(int j = 0; j < m; j++){
      if(col[i][j] < 10)
        cout << "  " << col[i][j];
      else
        cout << ' ' << col[i][j];
    }
    cout << '\n';
  }
}

void set_col(){
  for(int j = 0; j < m; j++){
    col[0][j] = mapa[0][j] == '.' ? 1 : 0;
    for(int i = 1; i < n; i++){
      if(mapa[i][j] == '.')
        col[i][j] = 1+col[i-1][j];
      else
        col[i][j] = 0;
    }
  }
}

void update(int x, int y){
  if(pos[x][y])
    return;
  int i = x;
  int j = y;
  for(; i >= 0 && !pos[i][j]; i--)
    for(; j >= 0 && !pos[i][j]; j--)
      pos[i][j] = true;
}

void solve(){
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      update(i, j);
}

int main(){
  ios_base::sync_with_stdio(false);

  cin >> n >> m;
  for(int i = 0; i < n; i++)
    for(int j = 0; j < m; j++)
      cin >> mapa[i][j], pos[i][j] = false;
  set_col();
  set_row();
  int k, x, y;
  cin >> k;

  for(int i = 0; i < k; i++){
    cin >> x >> y;
    moveis[i].c = x;
    moveis[i].l = y;
  }
  solve();
  vizualize();
  sort(moveis, moveis+k, cmp);
  for(int i = 0; i < k; i++){
    if(pos[moveis[i].c][moveis[i].l] || pos[moveis[i].l][moveis[i].c]){
      cout << moveis[i].c << ' ' << moveis[i].l << '\n';
      return 0;
    }
  }

  return 0;
}
