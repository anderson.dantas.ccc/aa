#include <bits/stdc++.h>
#define N_MAX 10000010
#define ull unsigned long long
using namespace std;

int b[N_MAX];
int n;

void update(int i, int v){
  while(i <= n){
    b[i] += v;
    i += (i & (-i));
  }
}

ull query(int i){
  ull sum = 0;
  while(i){
    sum += b[i];
    i -= (i & (-i));
  }
  return sum;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin >> n;
  memset(b, 0, sizeof(b));
  for(int i = 1; i <= n; i++)
    update(n - i+1,  1);
  int j;
  for(int i = 1; i <= n; i++){
    cin >> j;
    cout << query(j);
      update(j+1, -1);
    if (i != n)
      cout << ' ';
  }
  cout << '\n';

  return 0;
}
