#include <bits/stdc++.h>
#define N_MAX 410
#define INF 99999999LL
#define ll long long
using namespace std;

ll cnt[N_MAX][N_MAX];
ll resp[N_MAX][N_MAX][N_MAX];
ll d[26][26];
ll dp[26][26];
string s;

ll cost(int l, int r){
  if(cnt[l][r] != INF)
    return cnt[l][r];
  int acum = 0;
  for(int i = 0; i < (r-l+1)>>1; i++){
    acum += dp[s[l+i]-'a'][s[r-i]-'a'];
  }
  return cnt[l][r] = acum;
}

ll solve(int l, int r, int k){
  if(resp[l][r][k] != INF)
    return resp[l][r][k];
  if(k == 1 || r == l)
    return cost(l, r);
  ll ans = INF;
  for(int i = 0; i < r-l; i++)
    ans = min(ans, cost(l, l+i) + solve(l+i+1, r, k-1));
  return resp[l][r][k] = ans;
}

int main(){
  ios_base::sync_with_stdio(false);
  for(int i = 0; i < N_MAX; i++)
    for(int j = i+1; j < N_MAX; j++){
      cnt[i][j] = INF;
      for(int k = 1; k < N_MAX; k++)
         resp[i][j][k] = INF;
    }
  for(int i = 0; i < 26; i++)
    for(int j = i+1; j < 26; j++)
      d[j][i] = d[i][j] = dp[j][i] = dp[i][j] = INF;
  for(int i = 0; i < 26; i++)
    for(int j = i+1; j < 26; j++)
      d[j][i] = d[i][j] = min(abs(j-i), abs(26-j)+i);
  for(int i = 0; i < 26; i++)
    for(int j = 0; j < 26; j++)
      for(int l = 0; l < 26; l++){
        dp[i][j] = min(dp[i][j], min(d[i][j], d[j][i]));
        dp[i][j] = min(d[i][j], d[i][l] + d[j][l]);
      }

  int n, k;
  cin >> n >> k >> s;
  cout << solve(0, n-1, k) << '\n';

  return 0;
}
